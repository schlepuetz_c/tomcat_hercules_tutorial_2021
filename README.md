# TOMCAT tutorial for the 2021 HERCULES school

Welcome to the git repository of the TOMCAT online tutorial material for the
2021 HERCULES school.

# How to run the tutorial
The tutorial is written as a python jupyter notebook. There are several ways
you can run the tutorial, either by installing a proper python environment
locally or by making use of the binder service (https://mybinder.org/) to run
the notebook on a remote service in the browser window.

## Option 1: Running the tutorial in your browser

Click the below Binder image to launch a Jupyter Notebook with the tutorial
without the need to download or install anything locally on your machine.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.psi.ch%2Fschlepuetz_c%2Ftomcat_hercules_tutorial_2021/master?filepath=Hercules_tutorial_2021.ipynb)

Note that it will take a few minutes to set up the remote python environment,
so please be patient.


## Option 2: Running the notebook locally


### Prerequisites
In order to run the notebook locally on your computer, you need to set up a
python environment with the necessary packages. At the moment, these include

* python=3
* h5py
* jupyter
* matplotlib
* numpy
* scikit-image


### Download the tutorial

Download the tutorial either by cloning the git repository (requires a local
git installation)

```bash
git clone https://gitlab.psi.ch/schlepuetz_c/tomcat_hercules_tutorial_2021.git
```

or by downloading the zip file with the latest version:

https://gitlab.psi.ch/schlepuetz_c/tomcat_hercules_tutorial_2021/-/archive/master/tomcat_hercules_tutorial_2021-master.zip


### Installing conda

The easiest way to create a dedicated python environment is by installing
Anaconda (https://anaconda.org/) or miniconda
(https://docs.conda.io/en/latest/miniconda.html) on your system. This provides
you with the conda environment management system.

### Install and activating the tutorial environment
This step is optional, but if you like, you can install a dedicated conda
environment for this tutorial that comes with the correct packages already
configured.

#### Using the command line

Locate the `environment.yml` file in this repository and run the following
command:

```bash
conda env create -f environment.yml
```
This will create a new environment called `tomcat_hercules_tutorial`. Once the
installation has finished, you then need to activate this environment:

```bash
conda activate tomcat_hercules_tutorial
```

#### Using Anaconda-Navigator

When using the Anaconda-Navigator, you can also install the new environment via
the GUI interface. Open the Anaconda-Navigator application, then select the
*Environments* tab in the left navigation bar. Click on the *Import* button
at the bottom of the environments list.

In the dialog window, navigate to the `environment.yml` file. The name of the
environment will be inserted automatically. The click *Import* and wait for
the installation to finish.

Now you can always activate this environment by selecting it in the
environments list.


#### Start the jupyter notebook server

In the python environment you would like to use to run the tutorial, you now
need to start the jupyter notebook server:

```bash
jupyter notebook
```

If using the Anaconda-Navigator, you can also click on the *play* symbol next
to the tutorial environment and select *Open with Jupyter Notebook*.

Now either a browser will open automatically with the correct window, or you
have to copy and past the generated html link to your browser to open the
jupyter notebook service.

Inside the jupyter window in your browser, navigate to the
`Hercules_tutorial_2021.ipynb` and start the notebook.


