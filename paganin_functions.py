#!/usr/bin/env python
# coding: utf-8

__author__ = 'Federica Marone, Christian M. Schlepuetz'
__date_created__ = '2021-03-07'
__credits__ = ''
__copyright__ = 'Copyright (c) 2021, Paul Scherrer Institut'
__docformat__ = 'restructuredtext en'

import numpy
from skimage.transform import iradon
import matplotlib.pyplot as plt

def prepad_image(raw_image):
    """
    Apply constant padding to an image to make image dimensions even.

    Constant padding of 1 pixel width (repeating the value of the neighboring
    original pixel) is applied in all image dimensions with odd numbers of
    pixels to make it an even number.

    Even-numbered image dimensions are required as inputs for FFT transforms
    (Paganin filters).

    Parameters
    ----------
    raw_image : 2D array-like
        The input image to be padded to even dimensions

    Returns
    -------
    prepadded_image : 2D array-like
        The pre-padded image with even number of pixels in all dimensions.

    """

    # This method is needed if at least one dimension is odd, since the
    # pad_image can only handle even dimension. We additionally pad to the
    # right and/or lower, such that the number of left and/or upper padded
    # pixels remain as if for even dimension. This is important if selecting
    # back the original region
    isNumberOfXPixelsOdd = raw_image.shape[1] % 2
    isNumberOfYPixelsOdd = raw_image.shape[0] % 2

    prepadded_image = numpy.zeros((raw_image.shape[0] + isNumberOfYPixelsOdd,
                                  raw_image.shape[1] + isNumberOfXPixelsOdd))

    #prepadded image is now allocated and has even dimensions
    if isNumberOfXPixelsOdd and isNumberOfYPixelsOdd:
        prepadded_image[:-1,:-1] = raw_image
        #pad one row at lower
        prepadded_image[-1, :][:-1] = raw_image[-1, :]
        #pad one column at right
        prepadded_image[:, -1][:-1] = raw_image[:, -1]
        #lower right corner
        prepadded_image[-1, -1] = raw_image[-1, -1]

    elif isNumberOfXPixelsOdd:
        # Since the boolean check for "both dims odd" is done above, being
        # here means *only* isNumberOfXPixelsOdd
        prepadded_image[:,:-1] = raw_image
        #last column in prepadd equals last column in original
        prepadded_image[:,-1] = raw_image[:,-1]

    elif isNumberOfYPixelsOdd:
        # Since the boolean check for "both dims odd" is done above, being
        # here means *only* isNumberOfYPixelsOdd
        prepadded_image[:-1, :] = raw_image
        #last row in prepadded equals last row in original
        prepadded_image[-1, :] = raw_image[-1, :]
    else:
        prepadded_image = raw_image

    return(prepadded_image)

def pad_image(raw_image):
    """
    Apply a symmetric constant padding to make image dimensions a power of 2.

    This method only works on images with even-numbered dimensions. Use the
    :func:`prepad_image` function to ensure this.

    Parameters
    ----------
    raw_image : 2D array-like
        The input image to be padded to the next power of 2

    Returns
    -------
    padded_image : 2D array-like
        The padded image with the number of pixels in all dimensions being
        equal to a power of 2.

    """

    # only works for images with even dimensions
    numberOfXPixels = raw_image.shape[1]
    numberOfYPixels = raw_image.shape[0]

    # We just pad to the next power of 2 with constant padding
    padFactor = 0
    numberOfPaddedXPixels = (2**padFactor) * int(numpy.power(2.0,
        numpy.ceil(numpy.log2(numberOfXPixels + 1))))
    numberOfPaddedYPixels = (2**padFactor) * int(numpy.power(2.0,
        numpy.ceil(numpy.log2(numberOfYPixels + 1))))
    numberOfAddedXPixels = numberOfPaddedXPixels - numberOfXPixels
    numberOfAddedYPixels = numberOfPaddedYPixels - numberOfYPixels

    padded_image = numpy.zeros((numberOfPaddedYPixels, numberOfPaddedXPixels))

    xStart = numberOfAddedXPixels//2
    xStop  = numberOfAddedXPixels//2 + numberOfXPixels

    yStart = numberOfAddedYPixels//2
    yStop  = numberOfAddedYPixels//2 + numberOfYPixels
    padded_image[yStart:yStop, xStart:xStop] = raw_image[:,:]

    if numberOfAddedXPixels == 0 and numberOfAddedYPixels == 0:
        return padded_image

    #constant padding right
    padded_image[yStart:yStop, -numberOfAddedXPixels//2:] = \
        numpy.repeat(raw_image[: ,-1], numberOfAddedXPixels//2).reshape(
            (raw_image.shape[0], numberOfAddedXPixels//2))
    #constant padding left
    padded_image[yStart:yStop, : numberOfAddedXPixels//2] = \
        numpy.repeat(raw_image[: ,0] , numberOfAddedXPixels//2).reshape(
            (raw_image.shape[0], numberOfAddedXPixels//2))
    #constant padding up
    padded_image[: numberOfAddedYPixels//2, xStart:xStop] = \
        numpy.tile(raw_image[0, :] , numberOfAddedYPixels//2).reshape(
            (numberOfAddedYPixels//2, raw_image.shape[1]))
    #constant padding down
    padded_image[-numberOfAddedYPixels//2:, xStart:xStop] = \
        numpy.tile(raw_image[-1, :], numberOfAddedYPixels//2).reshape(
            (numberOfAddedYPixels//2, raw_image.shape[1]))
    #constant padding edges:upper left
    padded_image[0:numberOfAddedYPixels//2, 0:numberOfAddedXPixels//2] = \
        raw_image[0,0]
    #upper right
    padded_image[0:numberOfAddedYPixels//2, -numberOfAddedXPixels//2:] = \
        raw_image[0,-1]
    #lower left
    padded_image[-numberOfAddedYPixels//2:, 0:numberOfAddedXPixels//2] = \
        raw_image[-1,0]
    #lower right
    padded_image[-numberOfAddedYPixels//2:, -numberOfAddedXPixels//2:] = \
        raw_image[-1,-1]
    return padded_image

def paganin_filter(corrected_image, energy, pixel_size, delta, beta, distance,
                   prepad=False):
    """
    Apply the Paganin phase filter to a series of 2D projection images.

    Parameters
    ----------
    corrected_image : 3D array-like
        The set of flat and dark field-corrected projection images to which
        the Paganin phase filter is to be applied.
    energy : float
        The X-ray energy in units of keV.
    pixel_size : float
        The effective pixel size in units of meters.
    delta : float
        The real part of the refractive index at the measurement energy.
    beta : float
        The imaginary part of the refractive index at the measurement energy.
    distance : float
        The distance between the sample and detector in units of meters.
    prepad : bool, optional
        If set to True, apply the pre-padding step to the corrected image.
        (default = False)

    Returns
    -------
    filtered_image : 3D array-like
        The set of Paganin phase-filtered projection images.

    """

    # negative values in log (results in NaN)
    numpy.seterr(invalid='raise')
    # zero value in log (results in -Inf, i.e division by zero error)
    numpy.seterr(divide='raise')

    lamda = (1.23984193e-9)/energy #for photons: E = 1keV -> 1.23984193 nm

    if prepad:
        prepadded_image = prepad_image(corrected_image)
        paddedcorrected_image = pad_image(prepadded_image)
    else:
        paddedcorrected_image = pad_image(corrected_image)

    fftOfImage = numpy.fft.fft2(paddedcorrected_image)

    delta_x = pixel_size/(2*numpy.pi); delta_y = delta_x
    # quadratic pixels, pixel_size in meter
    # delta_x, delta_y remain after padding (fix pixel_size),
    # fftOfImage.shape takes care of additional (padded) pixels
    # The fftfreq(....) is necessary to give the proper k space units
    # (meter^{-1}), since lamda and distance are as well in meters,
    # otherwise the term 'distance*lamda*delta*k_squared' would have
    # inconsistent units. (The k units would be pixel_size^{-1})
    k_x = numpy.fft.fftfreq(fftOfImage.shape[1], d=delta_x)
    k_y = numpy.fft.fftfreq(fftOfImage.shape[0], d=delta_y)
    k_x_grid, k_y_grid = numpy.meshgrid(k_x, k_y)
    k_squared = k_x_grid**2 + k_y_grid**2
    paganinFilter = 1.0 / (1.0 + distance * lamda * delta * k_squared /
        (4 * numpy.pi * beta))
    fftfiltered_image = (numpy.fft.ifft2(paganinFilter*fftOfImage)).real
    try:
        filtered_image = numpy.log(fftfiltered_image)
    except FloatingPointError:
        # We should never be here since corrected_image should have reasonable
        # values. (Correction done in darkCorrectImages() and/or
        # flatCorrectImages()
        # machine epsilon, much bigger than tiny, but maybe more reasonable
        print("taking log of fft filtered images failed")
        threshold = numpy.finfo(numpy.float).eps
        criticalIndices = fftfiltered_image <= threshold
        print("Critical Indices: " + str(criticalIndices))
        print("replacing values smaller than " + str(threshold) + " with " +
            str(threshold))
        fftfiltered_image[criticalIndices] = threshold
        filtered_image = numpy.log(fftfiltered_image)

    filtered_image *= -(lamda/(4*numpy.pi*beta))
    # finished filtering
    numberOfLeftPaddedPixels =  (paddedcorrected_image.shape[1] -
        corrected_image.shape[1])//2
    numberOfUpperPaddedPixels = (paddedcorrected_image.shape[0] -
        corrected_image.shape[0])//2
    # Revert padding (select original region). This selection works also in
    # case that no padding was done
    # It is also correct for odd numbers, where /2 means "floor", this is ok
    # since prepadding padded to right and lower
    filtered_image = filtered_image[
        numberOfUpperPaddedPixels:numberOfUpperPaddedPixels + corrected_image.shape[0],
        numberOfLeftPaddedPixels:numberOfLeftPaddedPixels + corrected_image.shape[1]]
    return filtered_image

def paganin_2Dfilter(corrected_image, energy, pixel_size, delta, beta,
                     distance, prepad=False):
    """
    Apply the 2D Paganin phase filter to a series of 1D projection images.

    This filter produces an extended version of the 1D image by repeating its
    pixel values in the orthogonal direction and performs a 2D Paganin phase
    filtering on the result.

    Parameters
    ----------
    corrected_image : 2D array-like
        The set of flat and dark field-corrected 1D projection images to which
        the Paganin phase filter is to be applied.
    energy : float
        The X-ray energy in units of keV.
    pixel_size : float
        The effective pixel size in units of meters.
    delta : float
        The real part of the refractive index at the measurement energy.
    beta : float
        The imaginary part of the refractive index at the measurement energy.
    distance : float
        The distance between the sample and detector in units of meters.
    prepad : bool, optional
        If set to True, apply the pre-padding step to the corrected image.
        (default = False)

    Returns
    -------
    filtered_image : 2D array-like
        The set of Paganin phase-filtered 1D projection images.

    """

    # negative values in log (results in NaN)
    numpy.seterr(invalid='raise')
    # zero value in log (results in -Inf, i.e division by zero error)
    numpy.seterr(divide='raise')

    lamda = (1.23984193e-9)/energy #for photons: E = 1keV -> 1.23984193 nm

    if prepad:
        prepadded_image = prepad_image(corrected_image)
        paddedcorrected_image = pad_image(prepadded_image)
    else:
        paddedcorrected_image = pad_image(corrected_image)

    wantedVolSize = 11
    padFactor = 1
    volSize = (2**padFactor) * int(numpy.power(2.0,
        numpy.ceil(numpy.log2(wantedVolSize + 1))))
    print(volSize)
    sinoVol = numpy.zeros((volSize,paddedcorrected_image.shape[0],
        paddedcorrected_image.shape[1]))
    sinoVol[:] = paddedcorrected_image
    fftOfImage = numpy.fft.fft2(sinoVol, axes=(0,2))
    print (fftOfImage.shape)

    delta_x = pixel_size/(2*numpy.pi); delta_y = delta_x
    # quadratic pixels, pixel_size in meter
    # delta_x, delta_y remain after padding (fix pixel_size),
    # fftOfImage.shape takes care of additional (padded) pixels
    # The fftfreq(....) is necessary to give the proper k space units
    # (meter^{-1}), since lamda and distance are as well in meters,
    # otherwise the term 'distance*lamda*delta*k_squared' would have
    # inconsistent units. (The k units would be pixel_size^{-1})
    k_x = numpy.fft.fftfreq(fftOfImage.shape[0], d=delta_x); k_y = \
        numpy.fft.fftfreq(fftOfImage.shape[2], d=delta_y)
    k_z = numpy.linspace(0,1,fftOfImage.shape[1])
    _, k_x_grid, k_y_grid = numpy.meshgrid(k_z, k_x, k_y)
    k_squared = k_x_grid**2 + k_y_grid**2
    paganinFilter = 1.0 / (1.0 + distance * lamda * delta * k_squared /
        (4 * numpy.pi * beta))
    fftfiltered_image = (numpy.fft.ifft2(
        paganinFilter*fftOfImage, axes=(0,2))).real
    try:
        filtered_image = numpy.log(fftfiltered_image[int(volSize/2),:,:])
    except FloatingPointError:
        # We should never be here since corrected_image should have reasonable
        # values. (Correction done in darkCorrectImages() and/or
        # flatCorrectImages()
        print("taking log of fft filtered images failed")
        # machine epsilon, much bigger than tiny, but maybe more reasonable
        threshold = numpy.finfo(numpy.float).eps
        criticalIndices = fftfiltered_image <= threshold
        print("Critical Indices: " + str(criticalIndices))
        print("replacing values smaller than " + str(threshold) + " with " +
            str(threshold))
        fftfiltered_image[criticalIndices] = threshold
        filtered_image = numpy.log(fftfiltered_image[int(volSize/2),:,:])

    filtered_image *= -(lamda/(4*numpy.pi*beta))
    #finished filtering
    numberOfLeftPaddedPixels =  \
        (paddedcorrected_image.shape[1] - corrected_image.shape[1])//2
    numberOfUpperPaddedPixels = \
        (paddedcorrected_image.shape[0] - corrected_image.shape[0])//2
    # Revert padding (select original region). This selection works also in
    # case that no padding was done
    # It is also correct for odd numbers, where /2 means "floor", this is ok
    # since prepadding padded to right and lower
    filtered_image = filtered_image[
        numberOfUpperPaddedPixels:numberOfUpperPaddedPixels + corrected_image.shape[0],
        numberOfLeftPaddedPixels:numberOfLeftPaddedPixels + corrected_image.shape[1]]
    return filtered_image

def paganin_1Dfilter(corrected_image, energy, pixel_size, delta, beta,
    distance, prepad=False):
    """
    Apply A 1D Paganin phase filter to a series of 1D projection images.

    Parameters
    ----------
    corrected_image : 2D array-like
        The set of flat and dark field-corrected 1D projection images to which
        the Paganin phase filter is to be applied.
    energy : float
        The X-ray energy in units of keV.
    pixel_size : float
        The effective pixel size in units of meters.
    delta : float
        The real part of the refractive index at the measurement energy.
    beta : float
        The imaginary part of the refractive index at the measurement energy.
    distance : float
        The distance between the sample and detector in units of meters.
    prepad : bool, optional
        If set to True, apply the pre-padding step to the corrected image.
        (default = False)

    Returns
    -------
    filtered_image : 2D array-like
        The set of Paganin phase-filtered 1D projection images.

    """
    # negative values in log (results in NaN)
    numpy.seterr(invalid='raise')
    # zero value in log (results in -Inf, i.e division by zero error)
    numpy.seterr(divide='raise')

    lamda = (1.23984193e-9)/energy #for photons: E = 1keV -> 1.23984193 nm

    if prepad:
        prepadded_image = prepad_image(corrected_image)
        paddedcorrected_image = pad_image(prepadded_image)
    else:
        paddedcorrected_image = pad_image(corrected_image)

    fftOfImage = numpy.fft.fft(paddedcorrected_image, axis=1)

    delta_x = pixel_size / (2 * numpy.pi)
    # quadratic pixels, pixel_size in meter
    # delta_x, delta_y remain after padding (fix pixel_size),
    # fftOfImage.shape takes care of additional (padded) pixels
    # The fftfreq(....) is necessary to give the proper k space units
    # (meter^{-1}), since lamda and distance are as well in meters,
    # otherwise the term 'distance*lamda*delta*k_squared' would have
    # inconsistent units. (The k units would be pixel_size^{-1})
    k_x = numpy.fft.fftfreq(fftOfImage.shape[1], d=delta_x)
    k_y = numpy.linspace(0,1,paddedcorrected_image.shape[0])
    k_x_grid = numpy.meshgrid(k_x,k_y)[0]
    k_squared = k_x_grid**2
    paganinFilter = 1.0 / (1.0 + distance * lamda * delta * k_squared /
        (4 * numpy.pi * beta))

    fftfiltered_image = (numpy.fft.ifft(paganinFilter * fftOfImage)).real
    try:
        filtered_image = numpy.log(fftfiltered_image)
    except FloatingPointError:
        # We should never be here since corrected_image should have reasonable
        # values. (Correction done in darkCorrectImages() and/or
        # flatCorrectImages()
        print("taking log of fft filtered images failed")
        # machine epsilon, much bigger than tiny, but maybe more reasonable
        threshold = numpy.finfo(numpy.float).eps
        criticalIndices = fftfiltered_image <= threshold
        print("Critical Indices: " + str(criticalIndices))
        print("replacing values smaller than " + str(threshold) + " with " +
            str(threshold))
        fftfiltered_image[criticalIndices] = threshold
        filtered_image = numpy.log(fftfiltered_image)

    filtered_image *= -(lamda/(4*numpy.pi*beta))
    #finished filtering
    numberOfLeftPaddedPixels =  \
        (paddedcorrected_image.shape[1] - corrected_image.shape[1])//2
    numberOfUpperPaddedPixels = \
        (paddedcorrected_image.shape[0] - corrected_image.shape[0])//2
    # Revert padding (select original region). This selection works also in
    # case that no padding was done
    # It is also correct for odd numbers, where /2 means "floor", this is ok
    # since prepadding padded to right and lower
    filtered_image = filtered_image[
        numberOfUpperPaddedPixels:numberOfUpperPaddedPixels + corrected_image.shape[0],
        numberOfLeftPaddedPixels:numberOfLeftPaddedPixels + corrected_image.shape[1]]
    return filtered_image