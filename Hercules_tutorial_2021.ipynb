{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# HERCULES School 2021: TOMCAT online tutorial\n",
    "\n",
    "Contact and author information:\n",
    "* Federica Marone, federica.marone@psi.ch\n",
    "* Christian M. Schlepütz, christian.schlepuetz@psi.ch\n",
    "\n",
    "Beamline website:\n",
    "* https://www.psi.ch/en/sls/tomcat\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Load the necessary packages and prepare the python environment\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import time\n",
    "\n",
    "from skimage.filters import threshold_otsu\n",
    "\n",
    "from tomography_tutorial_functions import *\n",
    "from paganin_functions import *\n",
    "\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# About this notebook\n",
    "\n",
    "This jupyter notebook was put together with the intent to guide you through a few examples of tomographic reconstructions and to highlight some of the basic decisions and tradeoffs that have to be taken into account when planning to acquire tomographic imaging data. In later sections, there will also be plenty of opportunity to play around with the code and different data sets to try and test different things by yourself.\n",
    "\n",
    "## Running the code\n",
    "To run the code in each code cell, simply select the cell and click on the **Run** button in the toolbar above. Alternatively, click into the cell and press `SHIFT + ENTER`.\n",
    "\n",
    "## Available functions for the tomography tutorial\n",
    "We've prepared a few specialised functions for the tomography tutorial:\n",
    "\n",
    "| Function | Description |\n",
    "| :--- | :--- |\n",
    "| `adjust_rotation_center()` | Adjust the rotation center by shifting (and zero-padding) the sinograms. | \n",
    "| `correct_data()` | Apply dark and flat field correction to projection data. |\n",
    "| `load_data()` | Load a single slice (detector row) of data from a data exchange file. |\n",
    "| `paganin_1Dfilter()` | The Paganin phase contrast filter for a 1-dimensional detector (single slice case) |\n",
    "| `reconstruct()` | Calculate the reconstruction (inverse Radon transform) from sinogram and angle data. |\n",
    "| `reduce_exposure_time()` | Simulate a reduction of exposure time in the data set. |\n",
    "| `reduce_projections()` | Simulate a reduction of the number of exposures in the data set. |\n",
    "| `scan_time_series()` | Simulate a time-series measurement with different exposure times. |\n",
    "\n",
    "In addition to these functions, the whole suite of standard python commands is available as well, of course.\n",
    "\n",
    "## Displaying the help text for a function\n",
    "You can have the help text for any function displayed in separate frame in the browser simply by typing the function name (without the brackets) followed by a question mark \"`?`\" in a new cell. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# run this cell to display the help text for the 'reconstruct' function in a separate frame\n",
    "reconstruct?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This works also for most \"standard\" python functions, not only the above listed special functions that were prepared for this tutorial. So you can also get the function signature for numpy's average function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# run this cell to display the help text for numpy's average function.\n",
    "np.average?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 1: A guided walk through the first data set\n",
    "\n",
    "## The first reconstruction"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# define some parameters for the later processing\n",
    "binning = 2\n",
    "padding = 0.5\n",
    "\n",
    "# load the raw data from file\n",
    "prj, dark, white, theta = load_data('Data/ZF_21keV_650nm_40mm_slice500.h5', binning=binning)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The raw data needs to be corrected for the flat field illumination and the detector dark counts to produce a series of high-quality projection images (1-D in our case):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cpr = correct_data(prj, dark, white)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The flat and dark field-corrected projections give a spatially resolved measurement of the amount of transmitted intensity through the sample:\n",
    "\n",
    "$ \\mathrm{cpr}(x,y) = \\frac{I}{I_0}$.\n",
    "\n",
    "What we are actually interested in is a measure of the X-ray absorption coefficient $\\mu$. According to the Beer-Lambert law (see presentation), we have:\n",
    "\n",
    "$ \\frac{I}{I_0} = e^{-\\mu d}$,\n",
    "\n",
    "where $d$ is the thickness of the sample, and $\\mu$ the (average) absorption coefficient of the material. For a spatially inhomogeneous sample, the last term is replaced by an integration over the local absorption coefficient along the X-ray beam path.\n",
    "\n",
    "The exact deriavation of the reconstruction process is presented in the presentation or in general text books (see for example AC: Kak, M Slaney, “Principles of Computerized Tomographic Imaging”, IEEE Press 1988. http://www.slaney.org/pct/pct-toc.html). For our tutorial, it is important to realize that we are interested in reconstructing the spatially resolved map of absorption coefficients in the sample. So what we need as an input for the reconstruction is something that is proportional to $\\mu$:\n",
    "\n",
    "$ \\mu = -ln(\\frac{I}{I_0}) \\frac{1}{d} $.\n",
    "\n",
    "This is why we calculate the so-called sinogram from the negative logarithm of the cpr array:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# calculate the sinogram\n",
    "sino = -np.log(cpr)\n",
    "# show the sinogram image\n",
    "plt.figure(figsize=[12,6])\n",
    "plt.imshow(sino.T, cmap='gray')\n",
    "plt.xlabel('projection number (angle)')\n",
    "plt.ylabel('(binned) pixel coordinate')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can run the reconstruction on this sinogram image (using the inverse Radon transform, see https://scikit-image.org/docs/dev/auto_examples/transform/plot_radon_transform.html), and providing the measured theta angle for each of the projections. Note that the reconstruction process itself will take a while (the larger the input image, the longer the calculation...)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reco1 = reconstruct(sino, theta, padding=padding)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Plot the reconstructed image\n",
    "plt.figure(figsize=[9,9])\n",
    "plt.imshow(reco1, cmap='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fixing the rotation center\n",
    "As you can see, the above imaged does not look very clean, there are some strange artefacts in the reconstruction. In this case, they are caused by the fact that the rotation center of the tomographic rotation stage was not perfectly centered on the projection. Such an offset in the rotation center causes C-shaped artefacts, and their orientation depends on the direction in which the actual center is offset from the center of the detector. For the reconstructed image orientation used in this tutorial (as given by the `iradon` function in `skimage.filters`), the C-shapes are oriented in the following manner:\n",
    "\n",
    "* $\\cap \\Rightarrow$ the rotation center is too low, it needs to be increased\n",
    "* $\\cup \\Rightarrow$ the rotation center is too high, it needs to be lowered\n",
    "\n",
    "### Question 1\n",
    "How do we need to adjust the rotation center in the above reconstruction? Make it lower or higher?\n",
    "\n",
    "### Exercise 1\n",
    "Try to adjust the rotation center for the reconstruction until you get a good result. We will need this center value for the rest of Part 1 of this tutorial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Adjust the center value (replace \"None\" with an actual number) until you get a good reconstruction.\n",
    "# Look at the output of the above reconstruction function to get a good starting guess.\n",
    "center = None\n",
    "reco_c = reconstruct(-np.log(cpr), theta, center=center, padding=padding)\n",
    "plt.figure(figsize=[9,9])\n",
    "plt.imshow(reco_c, cmap='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Effect of aqcuisition parameters on image quality\n",
    "\n",
    "## Reduced exposure time\n",
    "One way to make a tomography scan faster is to decrease the exposure time for each projection. Of course, this will affect the signal to noise (SNR) in the reconstructed image. Below you can see an example of reducing the exposure times by a factor of 10 or 100 for the above reconstruction.\n",
    "\n",
    "(Note: all reconstructions are performed with the rotation center you determined above)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Simulate data sets with a reduced exposure time\n",
    "prj_e10, dark_e10, white_e10 = reduce_exposure_time(0.1, prj, dark, white)\n",
    "prj_e100, dark_e100, white_e100 = reduce_exposure_time(0.01, prj, dark, white)\n",
    "\n",
    "# Calculate the reconstructions for the data sets with lower exposure times\n",
    "cpr_e10 = correct_data(prj_e10, dark_e10, white_e10)\n",
    "reco_e10 = reconstruct(-np.log(cpr_e10), theta, center=center, padding=padding)\n",
    "cpr_e100 = correct_data(prj_e100, dark_e100, white_e100)\n",
    "reco_e100 = reconstruct(-np.log(cpr_e100), theta, center=center, padding=padding)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compare the reconstructions for the different exposure times\n",
    "plt.figure(figsize=[16,8])\n",
    "plt.subplot(1,3,1)\n",
    "plt.imshow(reco_c, cmap='gray')\n",
    "plt.title(\"original\")\n",
    "plt.subplot(1,3,2)\n",
    "plt.imshow(reco_e10, cmap='gray')\n",
    "plt.title(\"exposure reduced 10x\")\n",
    "plt.subplot(1,3,3)\n",
    "plt.imshow(reco_e100, cmap='gray')\n",
    "plt.title(\"exposure reduced 100x\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reduced number of projections\n",
    "Another method to reduce scan times is, of course, to reduce the overall number of projections that are acquired over 180 degrees. Also this has consequences for the image quality and the SNR:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "prj_n4, theta_n4 = reduce_projections(4, prj, theta)\n",
    "prj_n8, theta_n8 = reduce_projections(8, prj, theta)\n",
    "prj_n16, theta_n16 = reduce_projections(16, prj, theta)\n",
    "\n",
    "cpr_n4 = correct_data(prj_n4, dark, white)\n",
    "reco_n4 = reconstruct(-np.log(cpr_n4), theta_n4, center=center, padding=padding)\n",
    "cpr_n8 = correct_data(prj_n8, dark, white)\n",
    "reco_n8 = reconstruct(-np.log(cpr_n8), theta_n8, center=center, padding=padding)\n",
    "cpr_n16 = correct_data(prj_n16, dark, white)\n",
    "reco_n16 = reconstruct(-np.log(cpr_n16), theta_n16, center=center, padding=padding)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=[16,16])\n",
    "plt.subplot(2,2,1)\n",
    "plt.imshow(reco_c, cmap='gray')\n",
    "plt.title(\"original\")\n",
    "plt.subplot(2,2,2)\n",
    "plt.imshow(reco_n4, cmap='gray')\n",
    "plt.title(\"#projections reduced by 4x\")\n",
    "plt.subplot(2,2,3)\n",
    "plt.imshow(reco_n8, cmap='gray')\n",
    "plt.title(\"#projections reduced by 8x\")\n",
    "plt.subplot(2,2,4)\n",
    "plt.imshow(reco_n16, cmap='gray')\n",
    "plt.title(\"#projections reduced by 16x\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Phase reconstruction\n",
    "Making use of the partial coherence of the X-ray beams at the synchrotron, one can use the so-called single distance propagation-based phase contrast to boost the image contrast (see lecture and tutorial presentation). For the purpose of this tutorial, we use a 1-dimensional phase filter on our 1-dimensional detector images to calculate the phase-filtered projection images (fltp - **f**i**lt**ered **p**rojections). The filtering is based on some physical parameters (energy, distance, etc.), which have to be specified for the particular data acquisition conditions of the particular sample.\n",
    "\n",
    "The method used here is based on the so-called Paganin phase reconstruction as proposed by D. Paganin et al. (http://doi.wiley.com/10.1046/j.1365-2818.2002.01010.x)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Data acquisition parameters\n",
    "energy = 21.0\n",
    "pixel_size = 0.65e-6 * binning\n",
    "dist = 0.04\n",
    "\n",
    "# Sample properties\n",
    "delta = 2e-8\n",
    "beta = 1e-10\n",
    "\n",
    "# Calculate the phase-filtered projections\n",
    "fltp = paganin_1Dfilter(cpr, energy, pixel_size, delta, beta, dist, prepad=True)\n",
    "\n",
    "# The above phase filter step already includes taking the negative logarithm of\n",
    "# the measured transmitted intensities, so the sinogram is identical to the\n",
    "# filtered projections.\n",
    "sino_phase = fltp\n",
    "\n",
    "# Run the reconstruction of the phase-filtered sinograms\n",
    "reco_phase = reconstruct(sino_phase, theta, center=center, padding=0.7, filter='ramp')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the phase-reconstruction\n",
    "plt.figure(figsize=[9,9])\n",
    "plt.imshow(reco_phase, cmap='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The boost in image contrast in the phase reconstruction can be used to somewhat counter-act the loss of image quality when speeding up the tomography scans. This is used often when pushing the time-resolution towards faster and faster scan times for rapidly changing samples in a dynamic state.\n",
    "\n",
    "Below, we compare the reconstruction results for the original exposure time and for a 10x reduced exposure time per projection."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run the phase reconstructions on the above dataset with a 10x lower exposure time\n",
    "fltp_e10 = paganin_1Dfilter(cpr_e10, energy, pixel_size, delta, beta, dist, prepad=True)\n",
    "reco_phase_e10 = reconstruct(fltp_e10, theta, center=center, padding=0.7, filter='ramp')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Compare the results visually\n",
    "plt.figure(figsize=[16,16])\n",
    "plt.subplot(2,2,1)\n",
    "plt.imshow(reco_c, cmap='gray')\n",
    "plt.title(\"abs. reco: original exp.\")\n",
    "plt.subplot(2,2,2)\n",
    "plt.imshow(reco_phase, cmap='gray')\n",
    "plt.title(\"phase reco: original exp.\")\n",
    "plt.subplot(2,2,3)\n",
    "plt.imshow(reco_e10, cmap='gray')\n",
    "plt.title(\"abs. reco: exp. reduced 10x\")\n",
    "plt.subplot(2,2,4)\n",
    "plt.imshow(reco_phase_e10, cmap='gray')\n",
    "plt.title(\"phase reco: exp. reduced 10x\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Basic image analysis\n",
    "\n",
    "The reconstructed images are usually further analyzed to exctract quantitative information from the volume data sets (in our case, we just have one slice). Examples of such quantifications are:\n",
    "\n",
    "* packing density of granular materials\n",
    "* pore space volume in a porous medium\n",
    "* particle size distributions\n",
    "* etc.\n",
    "\n",
    "## Image thresholding / segmentation\n",
    "A first step in image analysis is very often the distinction between different phases (e.g. air vs. material) in the image. The simplest approach to perform this so-called segmentation is via automatically thresholding the grayscale image to produce a binary image where the zeros correspond to one phase and the ones to the other phase.\n",
    "\n",
    "For a quick visual explanation of the thresholding, and the function we will be using for it, see, for example, here:\n",
    "https://scikit-image.org/docs/dev/auto_examples/segmentation/plot_thresholding.html\n",
    "\n",
    "Let's try a quick thresholding operation on one of the reconstructed zebrafish scans. First, we display the image histogram to get a feeling for where the automatic threshold value was found:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculate the threshold values using the Otsu method\n",
    "auto_thresh_abs = threshold_otsu(reco_c)\n",
    "auto_thresh_phase = threshold_otsu(reco_phase)\n",
    "\n",
    "# Plot the resulting threshold values in the corresponding histograms\n",
    "plt.figure(figsize=[16,4])\n",
    "plt.subplot(1,2,1)\n",
    "h = plt.hist(np.ravel(reco_c), 256)\n",
    "plt.axvline(auto_thresh_abs, color='r')\n",
    "plt.title(\"Absorption reconstruction\")\n",
    "plt.subplot(1,2,2)\n",
    "h = plt.hist(np.ravel(reco_phase), 256)\n",
    "plt.axvline(auto_thresh_phase, color='r')\n",
    "plt.title(\"Phase reconstruction\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculate the segmented images\n",
    "segmented_abs = reco_c > auto_thresh_abs\n",
    "segmented_phase = reco_phase > auto_thresh_phase\n",
    "\n",
    "# Plot the results\n",
    "plt.figure(figsize=[16,16])\n",
    "plt.subplot(2,2,1)\n",
    "plt.imshow(reco_c, cmap='gray')\n",
    "plt.title(\"Absorption reconstruction\")\n",
    "plt.subplot(2,2,2)\n",
    "plt.imshow(reco_phase, cmap='gray')\n",
    "plt.title(\"Phase reconstruction\")\n",
    "plt.subplot(2,2,3)\n",
    "plt.imshow(segmented_abs, cmap='gray')\n",
    "plt.title(\"Abs. reco: automatic segmentation\")\n",
    "plt.subplot(2,2,4)\n",
    "plt.imshow(segmented_phase, cmap='gray')\n",
    "plt.title(\"Phase reco: automatic segmentation\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 2: The dose optimization challenge\n",
    "\n",
    "The guided walk in part 1 should have given you a basic impression of the different steps involved in reconstructing a tomographic data set and some of the quirks (rotation center) and experiment design choices (number of projections, exposure time, contrast mechanism) one encounters along the way.\n",
    "\n",
    "This second part of the tutorial is meant to be much more interactive you your part. Now it's play time! So here is the challenge.\n",
    "\n",
    "## Dose vs. image quality\n",
    "\n",
    "One important consideration when using X-rays for medical or diagnostic purposes or in conjunction with radiation-sensitive sampes is the total radiation dose imparted on the sample by a full CT scan. The dose is, of course, directly proportional to the total X-ray exposure time (number of projections $N$ times the exposure time per projection $t_p$). Usually, one wants to keep this dose as small as possible for any given scan. But as we have seen above, eventually the image quality will be compromised. So the fundamental challenge in X-ray imaging is always to get the best possible image quality for the lowest possible dose.\n",
    "\n",
    "## Exercise 2:\n",
    "\n",
    "In this exercise, you will perform the whole analysis pipeline from part 1 on a different sample. Here is what you should do:\n",
    "\n",
    "* From the below list of sample data sets, pick one of the files starting with \"uCT\" for the exercise (the \"ZF\" one is  the zebrafish we used above).\n",
    "* Reconstruct the data set to get a sense for the type of sample you have.\n",
    "* Now try to reduce the total X-ray exposure as much as possible while keeping a useable image quality, making use of the following tools from above:\n",
    "  * reduce the exposure time per projection (`reduce_exposure_time()`)\n",
    "  * reduce the number of projections (`reduce_projections()`)\n",
    "  * find a suitable set of phase reconstruction parameters, if desired (see note below)\n",
    "* Try a basic thresholding operation on the reconstruction result to check whether you are still able to get quantitative information from the sample.\n",
    "* Once you have found the best result for the smallest possible dose, calculate the dose reduction factor you have achieved with respect to the original data set.\n",
    "\n",
    "## Question 2:\n",
    "What is the dose reduction factor you were able to achieve?\n",
    "  \n",
    "  \n",
    "### Note for phase reconstructions\n",
    "The experimental parameters are fixed and the corresponding values are given in the below table for each samples. The only free parameters to influence the contrast in the reconstruction are the *beta* and *delta* parameters. While both of those are in principle adjustable, it is mostly the ratio of the two that has the biggest impact on the image contrast. Therefore, it is easiest if you fix one of the two (at around the values used in the example in Part 1), and you simply adjust the other one. This way, the whole optimization boils down to tweaking this one parameter.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## List of data sets\n",
    "\n",
    "Here are a few other datasets to test with the relevant acquisition parameters:\n",
    "\n",
    "| Dataset | Energy [keV] | Pixel size [um] | Distance [m] | # of pixels | # of projections | Description |\n",
    "| :--- | :---: | :---: | :---: | :---: | :---: | :--- |\n",
    "| uCT_ref_002_bamboo_01_slice650.h5 | 12.0 | 1.625 | 0.01 | 1850 | 1801 | Bamboo cocktail stick |\n",
    "| uCT_ref_007_match_02_slice800.h5 | 15.0 | 1.625 | 0.01 | 2270 | 1801 | match stick with head |\n",
    "| uCT_ref_009_white_patch_01_slice1140.h5 | 12.0 | 1.625 | 0.01 | 1280 | 1801 | Felt glider patch | \n",
    "| ZF_21keV_650nm_40mm_slice500.h5 | 21.0 | 0.65 | 0.04 | 980 | 2001 | Zebrafish embryo head |"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "binning = 2\n",
    "#prj, dark, white, theta = load_data('Data/uCT_ref_002_bamboo_01_slice650.h5')\n",
    "#prj, dark, white, theta = load_data('Data/uCT_ref_007_match_02_slice800.h5')\n",
    "prj, dark, white, theta = load_data('Data/uCT_ref_009_white_patch_01_slice1140.h5', binning=binning)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your code here...\n",
    "# (Copy-paste functions from Part 1 as much as you need.)\n",
    "# (Insert more cells as needed with the \"+\" button in the toolbar)\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 3: Tomoscopy\n",
    "*Tomoscopy* is a synonym for time-resolved tomography in dynamic systems, meaning systems that undergo temporal changes in their structure.\n",
    "\n",
    "The main challenge in time-resolved tomography is to match the time it takes to acquire a tomographic scan to the typical time scales in the system. The scan needs to be fast enough such that the structural changes within the sample are so small that it appears static during the scan. If this condition is not met, the scans are exhibiting motion artefacts that will render the data useless or at least obscure the dymanic events of interest.\n",
    "\n",
    "Just as in the dose optimization challenge, the goal is to keep the highest possible image quality at the necessary time resolution.\n",
    "\n",
    "## Bubble coalescence in a metal foam\n",
    "Certain metal alloys can form foams when heating them up and melting the ingredients. These materials are very lightweight and structurally solid, making them perfect for engineering.\n",
    "\n",
    "The formation of the bubbles and their evolution take place on the second to millisecond time scale. The rupture event of two bubbles merging into a single one lasts of the order of 1 ms or even less. The shape equilibration of the resulting larger bubble takes a few to a few tens of milliseconds.\n",
    "\n",
    "For this tutorial, we were granted access to the worlds fastest recorded tomography data set with micron resolution (many thanks to the group of F. Garcia-Moreno from the Helmholtz Zentrum Berlin, who have measured this dataset at the TOMCAT beamline). Details about the used setup can be found in a paper (https://dx.doi.org/10.1038/s41467-019-11521-1).\n",
    "\n",
    "We use here a sub-set of the complete data set that comprises a total of 75 ms of measurements. The fastest possible time-resolution is 1 ms. To simulate a choice of scan time, the `scan_time_series()` function used below will integrate the original scan data over the specified number of milliseconds. So, for example, we can either obtain 75 scans of 1 ms, or 25 scans of 3 ms each.\n",
    "\n",
    "You will note that the image quality is much poorer than for the above examples, but keep in mind that the scan was also aquired nearly 500'000 times faster! There are only 40 projections over 180 degrees, so you will see the familiar artefacts we observed in Part 1.\n",
    "\n",
    "## Acquisition parameters:\n",
    "* Energy: 24 keV\n",
    "* Distance: 250 mm\n",
    "* Pixel size: 2.75 um\n",
    "* The center is approximately at 263\n",
    "\n",
    "## Exercise 3:\n",
    "Explore the tomoscopy data set with the functions you mastered in the sections above. Try to play with simulating different scan times and how they affect the quality of the final reconstruction. There is indeed one very fast event hidden in the data set. Can you find it? Can you determine approximately at what time after the start of the time series the event happens?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "center = 263\n",
    "energy = 24.0\n",
    "pixel_size = 2.75e-6\n",
    "dist = 0.25"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Load the time-series data from file\n",
    "scan_time = 10 # simulated scan time in ms\n",
    "scan_data, dark, white, scan_theta = scan_time_series(scan_time=scan_time)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Note how scan_data and scan_theta are lists with one entry for every scan in the simulated time series:\n",
    "print(len(scan_data))\n",
    "print(scan_data[0].shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Select the projections and theta values for one time step\n",
    "scan_number = 2\n",
    "prj = scan_data[scan_number]\n",
    "theta = scan_theta[scan_number]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run a reconstruction on one of the simulated time points\n",
    "cpr = correct_data(prj, dark, white)\n",
    "reco_abs = reconstruct(-np.log(cpr), theta, center=center)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the reconstructed image\n",
    "plt.figure(figsize=[6,6])\n",
    "plt.imshow(reco_abs, cmap='gray')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Try a phase reconstruction\n",
    "delta = 2e-7\n",
    "beta = 1e-9\n",
    "fltp = paganin_1Dfilter(cpr, energy, pixel_size, delta, beta, dist, prepad=True)\n",
    "reco_phase = reconstruct(fltp, theta, center=center, padding=0.7, filter='ramp')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the phase reconstruction\n",
    "plt.figure(figsize=[6,6])\n",
    "plt.imshow(reco_phase, cmap='gray')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your code here..."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
