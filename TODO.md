# TODO:

## Bug fixes, problems

- Paganin filter not doing anything? Problem when using binned data? Probably
  yes, as the edge enhancement gets lost in the binning step!
- Find better set of test data:
    - small number of pixels to avoid need for binning
    - large enough propagation distance for obvious phase reconstruction
    - tomoscopy: Create a dataset with know angle readback values. Maybe even
      in an interlaced fashion, such that successive data sets nicely
      complement each other in angular space? Golden ratio step between
      successive data sets? Calculate rotation speed such that golden ratio
      of angular step size is missing at the end?

## Enhancements, Ideas

- Make use of ipywidgets to provide a GUI-like interface for the most basic
  tests?
